const express = require('express');
const homepage = require('./homepage');

const server = express();

const title = "My awesome webpage"

server.get('/', (req, res) => {
  const sourceIp = req.ip.split(':')[3];
  res.send(homepage(title, sourceIp));
});

server.listen(3000, function myFunc() {
  console.log('Server is listening on port 4242');
})