module.exports = (title, sourceIp) => `
  <html>
  <head>
    <title>${title}</title>
    <style>
      body {
        text-align: center;
        color: red;
        background-color: black;
      }
      p {
        color: orange;
      }
      #IP {
        color: white;
        border: red 2px solid;
      }
    </style>
  </head>
  <body>
    <h1>This is an h1 heading</h1>
    <p>This is a new line</p>
    <p>--------------  Newest line  --------------</p>
    <p>--------------  Newest line  --------------</p>
    <p>--------------  Newest line  --------------</p>
    <p>--------------  Newest line  --------------</p>
    <p>--------------  Newest line  --------------</p>
    <p>--------------  Newest line  --------------</p>
    <p>This is a p tag block</p>
    <p>Webpage name: ${title}</p>
    <p id="IP" >Your IP is: ${sourceIp}</p>
  </body>
  </html>
`